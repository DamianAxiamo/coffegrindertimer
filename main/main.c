/* Coffee grinder timer by damian.weber@axiamo.com
 * CC0 license
*/
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "esp_sleep.h"

#define GPIO_MOTOR 5        // motor solid state relay output
#define GPIO_BUTTON 4       // button input

#define TASK_MS 100         // 10Hz = 0.1s resolution
#define MS_HOLD_MANUAL 500  // select manual mode when press and hold longer than 0.5s
#define MS_WAIT_AFTER 2000  // after dispensing complete, can subsequently add some more manually while 2s
#define MS_MIN_AUTO 1750    // we have this time to increase to a double (2 taps) or even to enable program mode
#define MS_HOLD_PROG 500    // in auto mode, after taps we can hold to enable program mode for the selected portion
#define MS_CONFIRM_PROG 1500// after holding for programming the new time we need to tap once to confirm for the value to be saved to flash

#define USE_DEEP_SLEEP 1

enum FsmState {
    STATE_IDLE,
    STATE_PRESSED,
    STATE_MANUAL,
    STATE_AUTO,
    STATE_PROG_CONFIRM,
    STATE_WAIT_IDLE
} state = STATE_IDLE;

uint32_t time1 = 8000;  // default single
uint32_t time2 = 14000; // default double
uint32_t newTime = 0;

bool motorOn = false;
int stateMS = 0;
int holdTime = 0;
bool doProg = false;
bool isDouble = false;

void printTimes() {
    printf("single: %.1fs\tdouble: %.1fs\n", time1 / 1000.0, time2 / 1000.0);
}

static void setNewState(int new) {
    state = new;
    stateMS = 0;
    switch(new) {
    case STATE_IDLE:
        printf("\nready for some more coffee.\n");
        printTimes();
        break;
    case STATE_MANUAL:
        printf("manual mode selected.\n");
        break;
    case STATE_AUTO:
        printf("automatic mode selected. dispensing for a single...\n");
        break;
    default:
        break;
    }
}

void readValues() {
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    ESP_ERROR_CHECK( err );
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        err = nvs_get_u32(my_handle, "time1", &time1);
        err = nvs_get_u32(my_handle, "time2", &time2);
        switch (err) {
        case ESP_OK:
            printTimes();
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The value is not initialized yet!\n");
            break;
        default :
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }
        // Close
        nvs_close(my_handle);
    }
}

void writeValues() {
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    ESP_ERROR_CHECK( err );
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        err = nvs_set_u32(my_handle, "time1", time1);
        err = nvs_set_u32(my_handle, "time2", time2);
        nvs_close(my_handle);
    }
}

static void stepFSM(bool pressed, bool edge) {
    switch(state) {
    case STATE_IDLE:
        // wait for some action
#if USE_DEEP_SLEEP
        if(!pressed) {
            printf("entering deep sleep...\n");
            esp_sleep_enable_ext0_wakeup(GPIO_BUTTON, 1);
            esp_deep_sleep_start();
        }
#else
        if(edge && pressed) {
            setNewState(STATE_PRESSED);
            printf("let's go!\n");
            motorOn = true;
        }
#endif
        break;
    case STATE_PRESSED:
        // turn motor on, not yet clear in what mode we will run
        if(stateMS >= MS_HOLD_MANUAL) {
            setNewState(STATE_MANUAL); // manual mode
        } else if(!pressed) {
            setNewState(STATE_AUTO);
            holdTime = 0;
            doProg = false;
            isDouble = false;
        }
        break;

    case STATE_MANUAL:
        // dispense until released
        printf("manual time: %.1fs\n", (stateMS + MS_HOLD_MANUAL) / 1000.0);
        if(!pressed) {
            motorOn = false;
            setNewState(STATE_WAIT_IDLE);
            printf("manual mode finished\n");
        }
        break;

    case STATE_AUTO:
        // normal auto mode: two taps within MS_MIN_AUTO give a double. time can be aborted with a further press
        // to program time, tap once (single) or twice (double) and press-and-hold until desired time. after this tap to confirm program
        if(!doProg) {
            int time = (isDouble ? time2 : time1);
            printf("time remaining for a %s is %.1fs\n", isDouble ? "double" : "single", (time - stateMS) / 1000.0);
            if(pressed) {
                holdTime += TASK_MS;
                if(holdTime > MS_HOLD_PROG) {
                    doProg = true;
                }
            } else {
                holdTime = 0;
            }

            if(stateMS <= MS_MIN_AUTO) {
                if(edge && !pressed) {
                    if(!isDouble) {
                        // we need a double
                        isDouble = true;
                        printf("...selected a double\n");
                    } else {
                        // we are just pressing wildly, abort...
                        motorOn = false;
                        printf("...abort due to wild pressing\n");
                        setNewState(STATE_IDLE);
                    }
                }
            } else if(stateMS >= time) {
                // auto mode elapsed
                motorOn = false;
                setNewState(STATE_WAIT_IDLE);
                printf("...auto time exceeded\n");

            } else if(pressed) {
                // auto mode abort before elapsed
                motorOn = false;
                printf("...auto time stopped manually after %.1fs\n", stateMS / 1000.0);
                setNewState(STATE_IDLE);
            }
        } else {
            // programming mode
            if(edge && !pressed) {
                motorOn = false;
                if(stateMS > MS_MIN_AUTO) {
                    printf("done for a %s with time %.1fs\n", isDouble ? "double" : "single", stateMS / 1000.0);
                    printf("please confirm with a tap!\n");
                    newTime = stateMS;
                    setNewState(STATE_PROG_CONFIRM);
                } else {
                    printf("time too short! aborting!\n");
                    setNewState(STATE_IDLE);
                }
            } else {
                printf("programming time (%s): %.1fs\n", isDouble ? "double" : "single", stateMS / 1000.0);
            }
        }

        break;
    case STATE_PROG_CONFIRM:
        if(stateMS <= MS_CONFIRM_PROG) {
            if(edge && !pressed) {
                printf("confirmed. writing new value to flash. time: %.1fs for %s\n", newTime / 1000.0, isDouble ? "double" : "single");
                if(isDouble) {
                    time2 = newTime;
                } else {
                    time1 = newTime;
                }
                writeValues();
                setNewState(STATE_IDLE);
            }
        } else {
            printf("not confirmed, dropping time!\n");
            setNewState(STATE_IDLE);
        }
        break;
    case STATE_WAIT_IDLE:
        // we have the chance to add some more at the end
        if(edge) {
            if(pressed) {
                printf("...gimme some more\n");
                motorOn = true;
            } else {
                printf("...enough?\n");
                motorOn = false;
                setNewState(STATE_WAIT_IDLE);
            }
        }
        if(pressed) {
            printf("plus time %.1fs\n", stateMS / 1000.0);
        } else if(stateMS > MS_WAIT_AFTER) {
            // we do not need more, go back to idle
            printf("enough for now.\n");
            setNewState(STATE_IDLE);
        }
        break;
    }
    stateMS += TASK_MS;
}

void app_main(void) {
    bool pressed = false;
    // Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    gpio_config_t io_conf;
    // button input
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = 1ULL<<GPIO_BUTTON;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pull_down_en = 1;
    gpio_config(&io_conf);
    // motor output
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_MOTOR;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    gpio_config(&io_conf);

    readValues();

    if(esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT0) {
        motorOn = true;
        setNewState(STATE_PRESSED);
    } else {
        // first boot
        setNewState(STATE_IDLE);
    }

    while (1) {
        bool pressed_old = pressed;
        while(1) {
            pressed = gpio_get_level(GPIO_BUTTON);
            bool edge = false;
            if(pressed != pressed_old) {
                pressed_old = pressed;
                edge = true;
            }
            stepFSM(pressed, edge);
            gpio_set_level(GPIO_MOTOR, motorOn);
            vTaskDelay(pdMS_TO_TICKS(TASK_MS));
        }
    }
}
