# ESP32 based coffee grinder timer
Author: damian.weber@axiamo.com
Licence: CC0. use as you like without any guarantee :)

# Function
- Manual mode: press and hold until desired amount is dispensed. add more by press and hold again
- Auto mode: single or double tap for single or double amount dispensing. abort with a tap. add more when time elapsed by press and hold again
- Programming: single or double tap to select then press and hold until desired amount is dispensed. confirm after finished with a tap to save time to flash
